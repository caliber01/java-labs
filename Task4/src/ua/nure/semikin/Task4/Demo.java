package ua.nure.semikin.Task4;

public class Demo {
	
	public static void main(String[] args){
		System.out.println("Part1");
		System.out.println(Constants.SEPARATOR);
		Part1.main(new String[]{});
		System.out.println(Constants.SEPARATOR);
		System.out.println("Part2");
		System.out.println(Constants.SEPARATOR);
		Part2.main(new String[]{});
		System.out.println(Constants.SEPARATOR);
		System.out.println("Part3");
		System.out.println(Constants.SEPARATOR);
		Part3.main(new String[]{});
		System.out.println(Constants.SEPARATOR);
		System.out.println("Part4");
		System.out.println(Constants.SEPARATOR);
		Part4.main(new String[]{});
		System.out.println(Constants.SEPARATOR);
		System.out.println("Part5");
		System.out.println(Constants.SEPARATOR);
		Part5.main(new String[]{});
		System.out.println(Constants.SEPARATOR);
		System.out.println("Part6");
		System.out.println(Constants.SEPARATOR);
		Part6.main(new String[]{});
		System.out.println(Constants.SEPARATOR);
	}

}
