package ua.nure.semikin.Task4;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Part2 {
	public static void main(String[] args) {
		roundData("data.csv", "rounded.csv");
	}

	public static void roundData(String sourceFile, String destFile) {
		Table source = new Table(sourceFile);
		System.out.println(source.toString());
		List<Test> tests = source.getTestList();

		int[] possibleMarks;
		for (Test test : tests) {
			possibleMarks = getPossibleResults(test.getNumberOfQuestions());
			Iterator<Map.Entry<String, Integer>> iterator = test.getMarks()
					.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) iterator
						.next();
				int originalMark = pair.getValue();
				int roundedMark = originalMark == -1 ? -1 : round(originalMark,
						possibleMarks);
				pair.setValue(roundedMark);
			}
		}
		Table dest = Table.fromTestList(tests);
		System.out.println(dest.toString());
		dest.generateFile(destFile);
	}

	public static int round(int original, int[] possibleResults) {

		for (int i = 0; i < possibleResults.length; i++) {
			if (original < possibleResults[i]) {
				if (i == 0) {
					return possibleResults[i];
				}
				int previousValue = possibleResults[i - 1];
				int nextValue = possibleResults[i];
				if (original - previousValue > nextValue - original) {
					return nextValue;
				} else {
					return previousValue;
				}
			}
		}

		return -1;

	}

	public static int[] getPossibleResults(int numberOfQuestions) {
		int[] results = new int[numberOfQuestions + 1];
		for (float i = 0; i <= numberOfQuestions; i++) {
			results[Math.round(i)] = Math.round(i / numberOfQuestions * 100);
		}
		return results;
	}
}