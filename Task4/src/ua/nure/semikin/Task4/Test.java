package ua.nure.semikin.Task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {
	private String name;
	private int numberOfQuestions;
	private Map<String, Integer> marks;
	
	public Test(){
		marks = new HashMap<String, Integer>();
	}
	public Test(String name, int numberOfQuestions, Map<String, Integer> marks){
		
		this.name = name;
		this.numberOfQuestions = numberOfQuestions;
		this.marks = marks;
		
	}

	public void addMark(String name, int mark) {
		marks.put(name, mark);
	}
	
	public int getMark(String studentName){
		return marks.get(studentName);
	}
	
	public List<String> getStudentNames(){
		return new ArrayList<String>(marks.keySet());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}

	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}

	public Map<String, Integer> getMarks() {
		return marks;
	}

	public void setMarks(Map<String, Integer> marks) {
		this.marks = marks;
	}

}
