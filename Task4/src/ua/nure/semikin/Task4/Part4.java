package ua.nure.semikin.Task4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class Part4 {

	public static void main(String[] args) {
		printAbsences("rounded.csv");
	}

	public static void printAbsences(String fileName) {
		Table table = new Table(fileName);
		List<Test> tests = table.getTestList();

		ArrayList<Pair<String, ArrayList<String>>> absences = new ArrayList<Pair<String, ArrayList<String>>>();
		for (Test test : tests) {
			
			Pair<String, ArrayList<String>> absence = 
					new Pair<String, ArrayList<String>>(test.getName(), new ArrayList<String>());
			// iterating through marks of this test
            for(Map.Entry<String, Integer> mark : test.getMarks().entrySet()){
                // absence found
                if(mark.getValue() == -1){
                   absence.getValue().add(mark.getKey()) ;
                }
            }
            absences.add(absence);
		}
		
		Collections.sort(absences, new Comparator<Pair<String, ArrayList<String>>>(){
			@Override
			public int compare(Pair<String, ArrayList<String>> o1,
					Pair<String, ArrayList<String>> o2) {
				return o2.getValue().size() - o1.getValue().size();
			}
		});
		
		for(Pair<String, ArrayList<String>> absence : absences){
			System.out.print(absence.getKey()+" ==> [");
			
			Collections.sort(absence.getValue(),new Comparator<String>(){

				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);
				}
			});
			
			for(String studentName : absence.getValue()){
				System.out.print(studentName+' ');
			}
			System.out.println("]");
				
		}
	}
}
