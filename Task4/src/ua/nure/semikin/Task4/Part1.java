package ua.nure.semikin.Task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Part1 {

	public static void main(String[] args) {
		generateTable();
	}

	public static void generateTable(){
		ArrayList<Test> tests = new ArrayList<Test>();
        Random random = new Random();
		for(int i = 1; i<= Constants.T; i++) {
			String testName = "Test_"+i;
			int numberOfQuestions = random.nextInt(8) + 7;

			HashMap<String, Integer> marks = new HashMap<String, Integer>();
			for(int j = 1; j<= Constants.K; j++) {
				String studentName = "Lastname_"+j;
				int mark = random.nextInt(100);
				mark = mark % 5 == 0 ? -1 : mark;
				marks.put(studentName, mark);
			}

			tests.add(new Test(testName, numberOfQuestions, marks));
		}
		
		Table table = Table.fromTestList(tests);
		table.generateFile("data.csv");
	}
	
}
