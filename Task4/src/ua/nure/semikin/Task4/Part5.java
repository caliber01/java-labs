package ua.nure.semikin.Task4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Part5 {
	public static void main(String[] args) {
		
		printStudentAbsences("rounded.csv");

	}

	public static void printStudentAbsences(String fileName) {
		Table table = new Table(fileName);
		List<Test> tests = table.getTestList();

		ArrayList<Pair<String, Pair<Integer, ArrayList<String>>>> studentAbsences = new ArrayList<Pair<String, Pair<Integer, ArrayList<String>>>>();

		List<String> studentNames = tests.get(0).getStudentNames();

		for (String studentName : studentNames) {
			ArrayList<String> testsMissed = new ArrayList<String>();
			for (Test test : tests) {
				if (test.getMark(studentName) == -1) {
					testsMissed.add(test.getName());
				}
			}
			int timesMissed = testsMissed.size();

			Pair<String, Pair<Integer, ArrayList<String>>> studentAbsence = new Pair<String, Pair<Integer, ArrayList<String>>>(
					studentName, new Pair<Integer, ArrayList<String>>(
							timesMissed, testsMissed));

			studentAbsences.add(studentAbsence);
		}

		Collections.sort(studentAbsences, new Comparator<Pair<String, Pair<Integer, ArrayList<String>>>>() {

			@Override
			public int compare(
					Pair<String, Pair<Integer, ArrayList<String>>> o1,
					Pair<String, Pair<Integer, ArrayList<String>>> o2) {
				return o2.getValue().getKey() - o1.getValue().getKey();
			}


		});

		for (Pair<String, Pair<Integer, ArrayList<String>>> studentAbsence : studentAbsences) {

			System.out.print(studentAbsence.getKey() + " ==> ");
			System.out.print(studentAbsence.getValue().getKey() + ": [");
			
			Collections.sort(studentAbsence.getValue().getValue());
			
			for(String missedTest : studentAbsence.getValue().getValue()){
				System.out.print(missedTest+" ");
			}
			
			System.out.println("]");

		}
	}

}
