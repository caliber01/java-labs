package ua.nure.semikin.Task4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Table {

	private Logger log = Logger.getLogger(Table.class.getSimpleName());
	private List<Row> rows;

	public Table() {
		rows = new ArrayList<Row>();
	}

	public Table(Collection<Row> rows) {

		this();
		this.rows.addAll(rows);

	}

	public Table(String fileName) {

		rows = new ArrayList<Row>();
		Reader reader = null;
		try {
			reader = new InputStreamReader(new FileInputStream(fileName), "UTF-8");

			StringBuilder content = new StringBuilder();
			int character = reader.read();
			for (; character != -1; character = reader.read()) {
				content.append((char) character);
			}
			String text = content.toString();

			String[] rowTexts = text.split(System.lineSeparator());

			for (String rowText : rowTexts) {
				List<String> cells = Arrays.asList(rowText.split(";", -1));
				cells = cells.subList(0, cells.size() - 1);
				rows.add(new Row(cells));
			}

		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
					log.log(Level.SEVERE, e1.getMessage(), e1);
				}
			}
		}
	}

	public static Table fromTestList(List<Test> tests) {

		Table table = new Table();

		// number of questions row
		ArrayList<String> numberOfQuestionsCells = new ArrayList<String>();
		numberOfQuestionsCells.add("");
		for (Test test : tests) {
			numberOfQuestionsCells.add(String.valueOf(test
					.getNumberOfQuestions()));
		}
		Table.Row numberOfQuestionsRow = new Table.Row(numberOfQuestionsCells);

		// names row
		ArrayList<String> testNamesCells = new ArrayList<String>();
		testNamesCells.add("");
		for (Test test : tests) {
			testNamesCells.add(test.getName());
		}
		Table.Row testNamesRow = new Table.Row(testNamesCells);

		// student rows
		ArrayList<Table.Row> studentRows = new ArrayList<Table.Row>();
		List<String> studentNames = tests.get(0).getStudentNames();
		Collections.sort(studentNames);

		for (String studentName : studentNames) {

			ArrayList<String> studentCells = new ArrayList<String>();
			studentCells.add(studentName);
			for (Test test : tests) {
				int markValue = test.getMarks().get(studentName);
				String mark = markValue == -1 ? "" : String.valueOf(markValue);
				studentCells.add(mark);
			}
			studentRows.add(new Table.Row(studentCells));
		}

		// adding
		table.addRow(numberOfQuestionsRow);
		table.addRow(testNamesRow);
		table.addRows(studentRows);

		return table;
	}

	public List<Test> getTestList() {
		ArrayList<Test> tests = new ArrayList<Test>();

		ArrayList<String> students = new ArrayList<String>();
		for (int i = 2; i < rows.size(); i++) {
			students.add(rows.get(i).getCells().get(0));
		}

		// iterating through columns (tests)
		for (int i = 1; i < rows.get(0).getCells().size(); i++) {

			Test test = new Test();
			test.setNumberOfQuestions(Integer.valueOf(rows.get(0).getCells()
					.get(i)));
			test.setName(rows.get(1).getCells().get(i));

			// iterating through cells in column (marks)
			for (int j = 2; j < rows.size(); j++) {
				String studentName = students.get(j - 2);
				String mark = rows.get(j).getCells().get(i);
				int markValue = mark.equals("") ? -1 : Integer.valueOf(mark);
				test.addMark(studentName, markValue);
			}

			tests.add(test);
		}

		return tests;
	}

	public void addRow(Row row) {
		rows.add(row);
	}

	public void addRows(Collection<Row> row) {
		rows.addAll(row);
	}

	public void generateFile(String fileName) {

		Writer writer = null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8");
			writer.write(toString());
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					log.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();

		for (Row row : rows) {
			result.append(row.toString()).append(System.lineSeparator());
		}

		return result.toString();

	}

	public static class Row {

		private List<String> cells;

		public Row(List<String> cells) {
			this.cells = new ArrayList<String>();
			this.cells.addAll(cells);
		}

		public List<String> getCells() {
			return cells;
		}

		@Override
		public String toString() {

			StringBuilder result = new StringBuilder();
			for (String cell : cells) {
				result.append(cell).append(';');
			}
			return result.toString();
		}
	}
}