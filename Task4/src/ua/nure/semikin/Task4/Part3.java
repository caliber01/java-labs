package ua.nure.semikin.Task4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Part3 {

	public static void main(String[] args) {
		printGrades("rounded.csv");
	}

	public static void printGrades(String fileName) {

		Table table = new Table(fileName);
		List<Test> tests = table.getTestList();
		List<String> studentNames = tests.get(0).getStudentNames();

		ArrayList<Pair<String, Integer>> averageMarks = new ArrayList<Pair<String, Integer>>();

		for (String studentName : studentNames) {
			int participated = 0;
			int totalMark = 0;
			for (int i = 0; i < tests.size(); i++) {
				int mark = tests.get(i).getMark(studentName);
				// if he passed the test
				if(mark != -1){
					participated++;
                    totalMark += mark;
                }
			}
			averageMarks.add(new Pair<String, Integer>(studentName, totalMark
					/ participated));
		}

		Collections.sort(averageMarks, new Comparator<Pair<String, Integer>>() {

			@Override
			public int compare(Pair<String, Integer> o1,
					Pair<String, Integer> o2) {
				return o2.getValue() - o1.getValue();
			}

		});
		
		for(Pair<String, Integer> mark : averageMarks) {
			System.out.println(mark.getKey()+" ==> "+mark.getValue());
		}

	}
}
