package ua.nure.semikin.Task4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Part6 {

	public static void main(String[] args) {

		printAverageMarks("rounded.csv");

	}

	public static void printAverageMarks(String fileName) {
		Table table = new Table(fileName);
		List<Test> tests = table.getTestList();

		ArrayList<Pair<String, Integer>> testAverages = new ArrayList<Pair<String, Integer>>();
		for (Test test : tests) {
			Collection<Integer> marks = test.getMarks().values();
			int totalMark = 0;
			int deviser = 0;
			for (int mark : marks) {
				if (mark != -1) {
					totalMark += mark;
					deviser++;
				}
			}
			int averageMark = totalMark / deviser;
			testAverages.add(new Pair<String, Integer>(test.getName(),
					averageMark));
		}
		Collections.sort(testAverages, new Comparator<Pair<String, Integer>>() {

			@Override
			public int compare(Pair<String, Integer> o1,
					Pair<String, Integer> o2) {
				int compareValue = o2.getValue() - o1.getValue();
				return compareValue == 0 ? o1.getValue().compareTo(
						o2.getValue()) : compareValue;
			}

		});

		for (Pair<String, Integer> testAverage : testAverages) {
			System.out.println(testAverage.getKey() + " ==> "
					+ testAverage.getValue());
		}
	}
}
