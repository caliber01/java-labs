package ua.nure.semikin.Task2;

public class Circle {
	public static void main(String[] args) {
		Circle c = new Circle(0,0,10);
		Circle c2 = new Circle(5,5,4);
		System.out.println(c.contains(9, 3));
		System.out.println(c.contains(c2));
	}

	private double x;
	private double y;
	private double radius;

	public Circle() {
		this(0, 0, 1);
	}

	public Circle(double x, double y, double radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public void move(double dx, double dy) {
		x = x + dx;
		y = y + dy;
	}

	public boolean contains(double x, double y) {
		return Math.pow((x - this.x), 2) + Math.pow((y - this.y), 2) < Math
				.pow(radius, 2);
	}

	public boolean contains(Circle other) {
		return (x - radius < other.x - other.radius)
				&& (x + radius > other.x + other.radius)
				&& (y + radius > other.y + other.radius)
				&& (y - radius < other.y - other.radius);
	}

	public void print() {
		System.out.println("Center.X: " + x + "; Center.Y: "
				+ y + "; Radius: " + radius);
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}
}
