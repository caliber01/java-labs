package ua.nure.semikin.Task2;

public class Matrix {

	public static void main(String[] args) {

		Matrix a = new Matrix(new int[][] { { 1, 3 }, { 2, 5 } });
		Matrix b = new Matrix(new int[][] { { 1, 5 }, { 3, 2 } });
		try {
			a.multiply(b);
		} catch (MatrixException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("_________________");
		a.print();
		try {
			a.add(b);
		} catch (MatrixException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("_________________");
		a.print();
		a.transpose();
		System.out.println("_________________");
		a.print();
	}

	private int[][] matrix;
	private int rows;
	private int columns;

	// constructors
	public Matrix(int[][] matrix) {
		this(matrix.length, matrix[0].length);
		for(int i = 0; i < matrix.length; i++){
			this.matrix[i] = matrix[i].clone();
		}
	}

	public Matrix(int rows, int columns) {
		matrix = new int[rows][columns];
		this.rows = rows;
		this.columns = columns;
	}

	// properties
	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public int getElement(int i, int j) {
		return matrix[i][j];
	}

	public void setElement(int i, int j, int value) {
		matrix[i][j] = value;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	// methods
	public void add(Matrix other) throws MatrixException {
		if (rows != other.getRows() || columns != other.columns) {
			throw new MatrixException("Ivalid matrices' sizes");
		}
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				matrix[i][j] += other.getElement(i, j);
			}
		}
	}

	public void multiply(int value) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				matrix[i][j] *= value;
			}
		}
	}

	public void multiply(Matrix other) throws MatrixException {
		if (columns != other.getRows()) {
			throw new MatrixException("Invalid matrices");
		}
		int[][] newMatrix = new int[rows][other.getColumns()];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				for (int k = 0; k < columns; k++) {
					newMatrix[i][j] += matrix[i][k]
							* other.getElement(k, j);
				}
			}
		}
		matrix = newMatrix;
	}

	public void transpose() {
		int[][] transposed = new int[columns][rows];
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				transposed[i][j] = matrix[j][i];
			}
		}
		matrix = transposed;
	}

	public void print() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.print(Integer.toString(matrix[i][j]) + " ");
			}
			System.out.println();
		}
	}

	public static class MatrixException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public MatrixException() {
			super();
		}

		public MatrixException(String message) {
			super(message);
		}

		public MatrixException(String message, Throwable cause) {
			super(message, cause);
		}

		/**
		 * @param cause
		 */
		public MatrixException(Throwable cause) {
			super(cause);
		}
	}

}
