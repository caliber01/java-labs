package ua.nure.semikin.Task2;

public enum ResizeType {
	NONE, INCREASE, DECREASE
};
