package ua.nure.semikin.Task2;

interface IVector {
	void clear();

	void add(Object element);

	Object get(int index);

	void remove(int index);

	void print();
}

public class Vector implements IVector {

	public static void main(String[] args) {
		Vector v = new Vector();
		for (int i = 0; i < 10; i++) {
			v.add(i);
		}
		v.print();
		for (int i = 0; i < 7; i++) {
			v.remove(1);
			v.print();
		}
	}

	private Object[] vector;
	private int reserve;
	private int length;

	public Vector() {
		this(5);
	}

	public Vector(int reserve) {
		vector = new Object[reserve];
		this.reserve = reserve;
		length = 0;
	}

	public int getLength() {
		return length;
	}

	@Override
	public void clear() {
		length = 0;
		vector = new Object[reserve];
	}

	@Override
	public void add(Object element) {
		vector[length++] = element;
		resize();
	}

	@Override
	public Object get(int index) {
		return vector[index];
	}

	@Override
	public void remove(int index) {
		for (int i = index + 1; i < length; i++) {
			vector[i - 1] = vector[i];
		}
		length--;
	}

	@Override
	public void print() {
		for (int i = 0; i < length; i++) {
			System.out.print(vector[i].toString() + " ");
		}
		System.out.println();
	}

	private ResizeType checkNeedResize() {
		int difference = vector.length - length;
		if (difference > reserve * 2) {
			return ResizeType.DECREASE;
		} else if (difference == 0) {
			return ResizeType.INCREASE;
		}
		return ResizeType.NONE;
	}

	private void resize() {
		ResizeType type = checkNeedResize();
		Object[] newVector;
		switch (type) {
		case INCREASE:
			newVector = new Object[vector.length + reserve];
			System.arraycopy(vector, 0, newVector, 0, length );
			vector = newVector;
			return;
		case DECREASE:
			newVector = new Object[vector.length - reserve];
			System.arraycopy(vector, 0, newVector, 0, length );
			vector = newVector;
			return;
		case NONE:
			return;
		}
	}

}
