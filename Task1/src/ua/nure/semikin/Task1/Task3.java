package ua.nure.semikin.Task1;

public class Task3 {
	public static void main(String[] args){
		int value = Integer.parseInt(args[0]);
		System.out.println(isPrime(value));
	}
	
	public static boolean isPrime(int n){
		for(int i = 2; i < n; i++){
			if(n%i==0){
				return false;
			}
		}
		return true;
	}
}
