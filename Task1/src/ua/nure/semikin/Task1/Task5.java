package ua.nure.semikin.Task1;

public class Task5 {

	public static void main(String[] args) {
		int value = Integer.parseInt(args[0]);
		System.out.println(countFortunate(value));
	}

	public static int countFortunate(int n) {

		if (n % 2 == 1) {
			return -1;
		}
		int count = 0;

		for (int i = (int) Math.pow(10, n - 1); i < (int) Math.pow(10, n); i++) {

			int leftRes = 0;
			int rightRes = 0;
			int j = 0, k = i;
			for (; j < n / 2; j++) {
				rightRes += k % 10;
				k = k / 10;
			}
			for (; j < n; j++) {
				leftRes += k % 10;
				k = k / 10;
			}
			count = leftRes == rightRes ? count + 1 : count;
		}
		return count;
	}
}
