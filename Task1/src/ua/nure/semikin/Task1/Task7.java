package ua.nure.semikin.Task1;

public class Task7 {
	public static void main(String[] args){
		int value = Integer.parseInt(args[0]);
		print(getPrimes(value));
	}
	public static int[] getPrimes(int length){
		int[] arr = new int[length];
		for(int i = 0, j = 0; j < length; i++){
			if(isPrime(i+1)){
				arr[j++] = i+1;
			}
		}
		return arr;
	}
	public static boolean isPrime(int n){
		for(int i = 2; i < n; i++){
			if(n%i==0){
				return false;
			}
		}
		return true;
	}
	public static void print(int[] array){
		for(int i : array){
			System.out.println(i);
		}
	} 
}
