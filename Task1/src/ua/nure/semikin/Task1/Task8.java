package ua.nure.semikin.Task1;

public class Task8 {
	public static void main(String[] args){
		int first = Integer.parseInt(args[0]);
		int second = Integer.parseInt(args[0]);
		printMatrix(getChess(first,second));
	}
	public static char[][] getChess(int n, int m){
		char[][] field = new char[n][m];
		boolean rowWhite = false;
		boolean white;
		for(int i = 0; i<n; i++){
			white = rowWhite;
			for(int j = 0; j < m; j++){
				if(white){
					field[i][j] = 'B';
				}
				else{
					field[i][j] = 'W';
				}
				white = !white;
			}
			rowWhite = !rowWhite;
		}
		return field;
	}
	public static void printMatrix(char[][] matrix){
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix[i].length; j++){
				System.out.print(matrix[i][j]);
			}
			System.out.print("\r\n");
		}
	}
}
