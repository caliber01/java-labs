package ua.nure.semikin.Task1;

public class Task6 {
	public static void main(String[] args){
		int value = Integer.parseInt(args[0]);
		print(fibbo(value));
	}
	public static int[] fibbo(int n){
		int[] arr = new int[n];
		arr[0] = 0;
		arr[1] = 1;
		for(int i = 2; i < n; i++){
			arr[i] = arr[i-1] + arr[i-2];
		}
		return arr;
	}
	public static void print(int[] arr){
		for(int i : arr){
			System.out.println(i);
		}
	}
}
