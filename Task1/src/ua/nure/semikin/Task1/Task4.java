package ua.nure.semikin.Task1;

public class Task4 {
	public static void main(String[] args){
		int value = Integer.parseInt(args[0]);
		System.out.println(fact(value));
	}
	
	public static int fact(int n){
		int res = 1;
		for(int i = 1; i <= n; i++){
			res *= i;
		}
		return res;
	}
}
