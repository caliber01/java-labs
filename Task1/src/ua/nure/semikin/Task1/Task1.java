package ua.nure.semikin.Task1;

public class Task1 {
	public static void main(String[] args){
		int first = Integer.parseInt(args[0]);
		int second = Integer.parseInt(args[1]);
		System.out.println(nod(first,second));
	}
	public static int nod(int a, int b){
		while(true){
			if(a<b){
				a = a + b;
				b = a - b;
				a = a - b;
			}
			if(a%b == 0){
				return b;
			}
			a = a%b;
		}
	}
}
