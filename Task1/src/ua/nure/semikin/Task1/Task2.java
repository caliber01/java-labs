package ua.nure.semikin.Task1;

public class Task2 {
	//37 = 10
	public static void main(String[] args){
		int value = Integer.parseInt(args[0]);
		System.out.println(count(value));
	}
	
	public static int count(int value){
		int res = 0;
		while(value != 0) {
			res += value%10;
			value = value / 10;
		}
		return res;
	}
}
