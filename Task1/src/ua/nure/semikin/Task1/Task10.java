package ua.nure.semikin.Task1;

public class Task10 {
	public static void main(String[] args){
		int value = Integer.parseInt(args[0]);
		print(binomTriangular(value));
	}
	public static int[][] binomTriangular(int n){
		int[][] arr = new int[n][];
		for(int i = 0; i < n; i++){
			arr[i] = new int[i+1];
			for(int j = 0; j < i+1; j++){
				arr[i][j] = binom(i,j);
			}
		}
		return arr;
	}
	public static int binom(int n, int k){
		return fact(n)/(fact(k)*fact(n-k));
	}
	public static int fact(int n){
		int res = 1;
		for(int i = 1; i <= n; i++){
			res *= i;
		}
		return res;
	}
	public static void print(int[][] matrix){
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix[i].length; j++){
				System.out.print(matrix[i][j]);
			}
			System.out.print('\n');
		}
	}
}
