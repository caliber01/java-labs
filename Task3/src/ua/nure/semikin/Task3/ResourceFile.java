package ua.nure.semikin.Task3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResourceFile {

	private static Logger log = Logger.getLogger(ResourceFile.class.getName());

	public static void main(String[] args) {
		BufferedReader br = null;
		try {
			System.setIn(new FileInputStream("data/resourceinput.txt"));
		} catch (FileNotFoundException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		}
		try {
			br = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
			return;
		}

		Pattern keyPattern = Pattern.compile("^([a-zA-Z]+)(?:\\s)([a-z]{2})$");
		Matcher matcher;
		while (true) {
			System.out.println("Enter the key (\"stop\" to quit)");
			try {
				String response = br.readLine();
				if (response != null) {
					if (response.equals("stop")) {
						break;
					}
					matcher = keyPattern.matcher(response);
					matcher.matches();

					String key = matcher.group(1);
					String lang = matcher.group(2);
					System.out.println(getValue(key, lang));
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
			break;
		}
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

	public static String getValue(String key, String lang) {
		Locale locale = null;

		switch (lang) {
		case "en":
			locale = new Locale("en");
			break;
		case "ru":
			locale = new Locale("ru");
			break;
		case "uk":
			locale = new Locale("uk");
			break;
		default:
			locale = new Locale("en");
		}
		ResourceBundle rb = ResourceBundle.getBundle(
				"ua.nure.semikin.Task3.locale.resources", locale);
		return rb.getString(key);

	}

}
