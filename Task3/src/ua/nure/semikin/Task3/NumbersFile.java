package ua.nure.semikin.Task3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NumbersFile {

	private static Logger log = Logger.getLogger(NumbersFile.class.getName());

	public static void main(String[] args) {
		NumbersFile f = new NumbersFile(
				"data/source.txt",
				"data/dest.txt");
		f.execute();
	}

	private File sourceFile;
	private File destFile;

	private int[] randInts;
	private int[] resultInts;

	public NumbersFile(String sourceFile, String destFile) {
		this.sourceFile = new File(sourceFile);
		this.destFile = new File(destFile);
		try {
			boolean success = this.sourceFile.createNewFile()
					&& this.destFile.createNewFile();
			if(!success){
				throw new IOException("File not created");
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		}

		randInts = new int[20];
		resultInts = new int[20];
	}

	public void execute() {
		initFile();
		fillFile();
		print();
		dispose();
	}

	private void initFile() {
		Writer fw = null;
		try {
			fw = new OutputStreamWriter(new FileOutputStream(sourceFile),
					"UTF-8");
			Random randGen = new Random();
			for (int i = 0; i < 20; i++) {
				int rand = randGen.nextInt(50);
				randInts[i] = rand;
				fw.write(rand + " ");
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			try {
				if (fw != null) {
					fw.close();
				}
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

	private void fillFile() {
		Reader fr = null;
		Writer fw = null;

		try {
			fr = new InputStreamReader(new FileInputStream(sourceFile), "UTF-8");
			int data = fr.read();
			StringBuilder content = new StringBuilder();
			for (; data != -1; data = fr.read()) {
				content.append((char) data);
			}
			String text = content.toString();

			String[] ints = text.split(" ");
			int[] numbers = new int[20];
			for (int i = 0; i < ints.length; i++) {
				numbers[i] = Integer.valueOf(ints[i]);
			}
			Arrays.sort(numbers);
			fw = new OutputStreamWriter(new FileOutputStream(destFile), "UTF-8");
			for (int i = 0; i < numbers.length; i++) {
				fw.write(numbers[i] + " ");
			}
			resultInts = numbers;
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			try {
				if (fw != null) {
					fw.close();
				}
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
		}

	}

	private boolean dispose() {
		return sourceFile.delete() && destFile.delete();
	}

	private void print() {
		for (int i = 0; i < randInts.length; i++) {
			System.out.print(randInts[i] + " ");
		}
		System.out.println();
		for (int i = 0; i < resultInts.length; i++) {
			System.out.print(resultInts[i] + " ");
		}
		System.out.println();
	}
}
