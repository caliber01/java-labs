package ua.nure.semikin.Task3;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserFilter {

	private static Logger log = Logger.getLogger(UserFilter.class.getName());

	public static void main(String[] args) {
		Reader mails = null;
		Reader groups = null;
		Writer users = null;
		Reader result = null;
		try {
			mails = new InputStreamReader(new FileInputStream(
					"data/mails.txt"), "UTF-8");
			groups = new InputStreamReader(new FileInputStream(
					"data/groups.txt"), "UTF-8");
			users = new OutputStreamWriter(new FileOutputStream(
					"data/users.txt"), "UTF-8");
			filter(mails, groups, users);

            if (users != null) {
                users.close();
                users = null;
            }

			result = new InputStreamReader(new FileInputStream(
					"data/users.txt"), "UTF-8");
            int data = result.read();
            StringBuilder content = new StringBuilder();
            for (; data != -1; data = result.read()) {
                content.append((char) data);
            }
            System.out.println(content.toString());

		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			try {
				if (mails != null) {
					mails.close();
				}
				if (groups != null) {
					groups.close();
				}
                if (users != null) {
                    users.close();
                }
				if (result != null) {
					result.close();
				}
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

	public static void filter(Reader mails, Reader groups, Writer users)
			throws IOException {

		String regexMail = "(?m)^(#?[a-zа-я]+)([0-9]+);([а-яa-z]+)([0-9]+)@(?:[a-z]+)\\.com$";
		String regexGroup = "(?m)^(#?[a-zа-я]+)([0-9]+);([а-яa-zA-ZА-Я]+)([0-9]+)";
		Map<String, String> mailTable = parse(mails, regexMail);
		Map<String, String> groupTable = parse(groups, regexGroup);
		for (Map.Entry<String, String> keyValuePair : mailTable.entrySet()) {
			String login = keyValuePair.getKey();
			String group = groupTable.get(login);
			String mail = keyValuePair.getValue();
			if (group != null) {
				users.write(login + ";" + mail + ";" + group
						+ System.lineSeparator());
			}
		}
	}

	private static Map<String, String> parse(Reader source, String regex)
			throws IOException {
		HashMap<String, String> result = new HashMap<String, String>();
		int data = source.read();
		StringBuilder content = new StringBuilder();
		while (data != -1) {
			content.append((char) data);
			data = source.read();
		}
		String text = content.toString();
		Matcher matcher = Pattern.compile(regex).matcher(text);
		while (matcher.find()) {
			String[] row = matcher.group().split(";");
			String login = row[0];
			if (login.charAt(0) != '#') {
				result.put(row[0], row[1]);
			}
		}
		return result;
	}
}
