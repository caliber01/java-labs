package ua.nure.semikin.Task3;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SentenceReader implements Iterable<String> {
	
	private static Logger log = Logger.getLogger(SentenceReader.class.getName());
	
	public static void main(String[] args) {
		SentenceReader sr = new SentenceReader(
				"data/sentences.txt");
		for (String sent : sr) {
			System.out.println(sent);
		}
	}

	private String filePath;

	public SentenceReader(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public Iterator<String> iterator() {
		try {
			return iterator(filePath);
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
			return null;
		}
	}

	public Iterator<String> iterator(String filePath)
			throws IOException {
		return new SentenceIterator(filePath);
	}

	private static class SentenceIterator implements Iterator<String> {

		private Reader fr;
		private boolean hasNext;

		public SentenceIterator(String filePath) throws IOException{
			fr = new InputStreamReader(new FileInputStream(filePath), "UTF-8");
			hasNext = true;
		}

		@Override
		public boolean hasNext() {
			return hasNext;
		}

		@Override
		public String next() {
			try {
				return getSentence();
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
				return null;
			}
		}
		
		@Override
		public void remove(){
			
		}
		

		private String getSentence() throws IOException {
			StringBuilder sentence = new StringBuilder();
			int data = fr.read();
			while ((char) data != '.' && data != -1) {
				sentence.append((char) data);
				data = fr.read();
			}
			if (data == -1) {
				hasNext = false;
			}
			return sentence.toString().trim();
		}

	}

}
