package ua.nure.semikin.Task3;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

enum Values {
	SUBJECT(1), TYPE(2), ROOM(3), GROUP(4), DATE(5), TIME(6);

	private int value;

	Values(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
};

public class ScheduleParser {

	private static Logger log = Logger.getLogger(ScheduleParser.class.getName());

	public static void main(String[] args) {
		Reader fr = null;
		try {
			fr = new InputStreamReader(new FileInputStream("data/table.csv"),
					"Windows-1251");
			String result = getSchedule(fr);
			System.out.println(result);
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

	public static String getSchedule(Reader input) throws IOException {
		StringBuilder result = new StringBuilder();
		List<Pair> pairs = parse(input);
		Collections.sort(pairs, new Comparator<Pair>() {

			@Override
			public int compare(Pair p1, Pair p2) {
				return p1.getDate().compareTo(p2.getDate());
			}

		});
		int day = 0;
		for (Pair p : pairs) {
			if (p.getDate().get(Calendar.DAY_OF_MONTH) != day) {
				result.append("------------------------"
						+ System.lineSeparator());
				day = p.getDate().get(Calendar.DAY_OF_MONTH);
			}
			result.append(p.toString());

		}
		return result.toString();
	}

	private static List<Pair> parse(Reader input) throws IOException {
		ArrayList<Pair> pairs = new ArrayList<Pair>();
		int data = input.read();
		StringBuilder content = new StringBuilder();
		for (; data != -1; data = input.read()) {
			content.append((char) data);
		}
		String text = content.toString();

		String regex =
				// Предмет
				"\"" + "([а-яa-zА-ЯA-ZіІїЇ_]*)" + "\\s" + "([а-яА-Я]+)"
				// Тип занятия
				+ "\\s" + "((?:[а-яА-Я_]+[0-9])|(?:[0-9]{2,5}+[а-я]?))"
				// Аудитория
				+ "\\s" + "([а-яА-ЯіІїЇ]+-[0-9]+-[0-9]+(?:,[0-9])*)"
				// Группа
				+ "\",\"" + "([0-9]{2}\\.[0-9]{2}\\.[0-9]{4})"
				// Дата
				+ "\",\"" + "([0-9]{2}\\:[0-9]{2}\\:[0-9]{2})"
				// Время
				+ "\"";
		Matcher m = Pattern.compile(regex).matcher(text);

		while (m.find()) {
			Pair pair = null;
			try {
				pair = new Pair(m.group(Values.ROOM.getValue()),
						m.group(Values.GROUP.getValue()), m.group(Values.DATE
								.getValue()), m.group(Values.TIME.getValue()),
						m.group(Values.SUBJECT.getValue()), m.group(Values.TYPE
								.getValue()));
			} catch (ParseException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
			pairs.add(pair);
		}
		return pairs;
	}
}

class Pair {

	private String room;
	private String group;
	private Calendar date;
	private String subject;
	private String type;

	public Pair() {
	}

	public Pair(String room, String group, String date, String time,
			String subject, String type) throws ParseException {
		this.room = room;
		this.group = group;

		String dateTime = date + " " + time;
		DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		Calendar cal = Calendar.getInstance();
		cal.setTime(format.parse(dateTime));
		this.date = cal;
		this.subject = subject;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Calendar getDate() {
		return date;
	}

	public String getFormattedDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date.getTime());
	}

	public String getFormattedTime() {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		return format.format(date.getTime());
	}

	@Override
	public String toString() {
		return String.format("%s ==> %s %s %s %s" + System.lineSeparator(),
				getFormattedDate(), getFormattedTime(), getSubject(),
				getType(), getRoom());
	}
}