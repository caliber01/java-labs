package ua.nure.semikin.Task3;

public class Demo {
	public static void main(String[] args){
		System.out.println("1) File Printer");
		System.out.println("----------------------------");
		FilePrinter.main(new String[]{});
		System.out.println("----------------------------");
		System.out.println("2) Numbers file");
		System.out.println("----------------------------");
		NumbersFile.main(new String[]{});
		System.out.println("----------------------------");
		System.out.println("3) File analyzer");
		System.out.println("----------------------------");
		FileAnalyzer.main(new String[]{});
		System.out.println("----------------------------");
		System.out.println("4) Sentence iterator");
		System.out.println("----------------------------");
		SentenceReader.main(new String[]{});
		System.out.println("----------------------------");
		System.out.println("5) Resource file");
		System.out.println("----------------------------");
		ResourceFile.main(new String[]{});
		System.out.println("----------------------------");
		System.out.println("6) User filter");
		System.out.println("----------------------------");
		UserFilter.main(new String[]{});
		System.out.println("----------------------------");
		System.out.println("7) Schedule parser");
		System.out.println("----------------------------");
		ScheduleParser.main(new String[]{});
		System.out.println("----------------------------");
	}
}
