package ua.nure.semikin.Task3;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FilePrinter {
	
	private static Logger log = Logger.getLogger(FilePrinter.class.getName());
	
	public static void main(String args[]) {
		try {
			printFile("data/new.txt");
		} catch (Exception e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	public static void printFile(String fileName) {
		
		Reader fr = null;

		try {
			fr = new InputStreamReader(new FileInputStream(fileName), "UTF-8");

			int data = fr.read();
			StringBuilder content = new StringBuilder();
			for (; data != -1; data = fr.read()) {
				content.append((char) data);
			}
			String text = content.toString();

			String[] words = text.split("[ " + System.lineSeparator() + "]");
			for (int i = 0; i < words.length; i++) {
				if (words[i].length() > 2) {
					words[i] = words[i].toUpperCase();
				}
				System.out.println(words[i]);
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		} finally {

			try {
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}
}
