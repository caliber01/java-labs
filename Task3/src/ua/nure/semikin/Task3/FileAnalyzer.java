package ua.nure.semikin.Task3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileAnalyzer {
	
	private static Logger log = Logger.getLogger(FileAnalyzer.class.getName());
	
	public static void main(String[] args) {

		FileAnalyzer fa = new FileAnalyzer(
				"data/types.txt");

		try {
			System.setIn(new FileInputStream("data/analyzerinput.txt"));
		} catch (FileNotFoundException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		}

		System.out.println("Insert the type");
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
			String response = br.readLine();
			String[] values = null;
			if (response != null) {
				values = fa.analyze(response);
				for (int i = 0; i < values.length; i++) {
					System.out.println(values[i]);
				}
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	private String fileName;

	public FileAnalyzer(String fileName) {
		this.fileName = fileName;
	}

	public String[] analyze(String type) throws IOException {
		Class<?> valueType = null;
		if (type.equals("Double")) {
			valueType = Double.class;
		} else if (type.equals("Integer")) {
			valueType = Integer.class;
		} else if (type.equals("Char")) {
			valueType = Character.class;
		} else if (type.equals("String")) {
			valueType = String.class;
		} else {
			return new String[] {};
		}
		return getValues(valueType);
	}

	private String[] getValues(Class<?> type) {
		Reader fr = null;
		try {
			fr = new InputStreamReader(new FileInputStream(fileName), "UTF-8");
			int data = fr.read();
			StringBuilder content = new StringBuilder();
			for (; data != -1; data = fr.read()) {
				content.append((char) data);
			}
			String text = content.toString();
			text = text.replaceAll(System.lineSeparator(), "");
			String[] values = text.split(" ");
			ArrayList<String> result = new ArrayList<String>();
			for (int i = 0; i < values.length; i++) {
				if (getClass(values[i]) == type) {
					result.add(values[i]);
				}
			}
			return result.toArray(new String[] {});
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
			return null;
		} finally {
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
		}

	}

	private static Class<?> getClass(String value) {
		if (isDouble(value)) {
			return Double.class;
		} else if (isInteger(value)) {
			return Integer.class;
		} else if (isChar(value)) {
			return Character.class;
		} else {
			return String.class;
		}
	}

	private static boolean isInteger(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException n) {
			return false;
		}
	}

	private static boolean isDouble(String value) {
		if (value.contains(".")) {
			try {
				Double.parseDouble(value);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		} else {
			return false;
		}
	}

	private static boolean isChar(String value) {
		return value.length() == 1;
	}

}
